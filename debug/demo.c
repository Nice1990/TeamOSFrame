#include "pthread.h"
#include "stdio.h"
#include "windows.h"

#include "../TeamOSFrame.h"

/* 模拟定时中断，线程 */
void tim_irq(void)
{
    while (1)
    {
        TOSF_TimerCB(10);
        Sleep(10);
    }
}

void tim_init(void)
{
    pthread_t thread1;
    char *message1 = "TIM2";
    int ret_thrd1;

    /* 模拟单片机中断 */
    ret_thrd1 = pthread_create(&thread1, NULL, (void *)&tim_irq, (void *)message1);
    if (ret_thrd1 != 0)
    {
        printf("thread create fault!");
        return;
    }
}

void task_t1(void)
{
    printf("task-1\n");

    // Sleep(2000); // 阻塞，测试抢占
}

void task_t2(void)
{
    printf("task-2\n");
}

void task_seize(void)
{
    printf("---seize---\n");
}

int main(void)
{
    printf("Demo Start:\n");

    /* 创建内存并初始化 */
    static type_TOSF_Task task_tab[5];
    TOSF_Init(task_tab, sizeof(task_tab) / sizeof(type_TOSF_Task));

    /* 定时器线程创建 */
    tim_init();

    /* 注册任务：任务函数、周期、优先级、任务名（可空字符串）；注册后返回任务ID号 */
    type_TaskId ID_Tim1s = TOSF_LoginTask(task_t1, 1000, 0, "T-1");
    TOSF_LoginTask(task_t2, 500, 1, "T-2");
    TOSF_LoginTask(task_seize, 600, TOSF_SEIZE_PRIORITY + 0, "Seize");

    while (1)
    {
        /* 非阻塞轮询调度 */
        TOSF_TaskHandler();

        /* 定时打印 */
#if 1
        static int t = 0;
        if (t < TOSF_GetSysTime())
        {
            t = TOSF_GetSysTime() + 1000;
            printf("\n\n t=%u \n\n", TOSF_GetSysTime());
        }
#endif
    }

    return 0;
}