# 协作式系统框架

1. 概要
    - 基于时间片管理，定时执行任务
    - 运行非阻塞式任务
    - 可自定义系统节拍
    - 可裁剪，基于宏定义
      - 任务信息统计打印功能
      - 系统时钟
      - 优先级+抢占功能
    - 系统启动ms时间查询、函数执行时间测量
    - 协作式 + 抢占式任务调度 (抢占式功能很局限)

2. 注意
    - 非阻塞任务
    - 使能优先级功能后有任务抢占功能
    - 优先级从TOSF_SEIZE_PRIORITY开始拥有抢占功能
    - 高优先级任务执行时间必须短，绝对不可以有延时函数等
    - 任务内存大小自定义，决定可创建任务数量

3. 使用说明
 
```c
/* Demo */

#include "TeamOSFrame.h"
int main(void)
{
    /* 创建内存并初始化 */
    static type_TOSF_Task task_tab[5];
    TOSF_Init(task_tab, sizeof(task_tab) / sizeof(type_TOSF_Task));

    /* 外设初始化 */
    Init();

    /* 注册任务：任务函数、周期、优先级、任务名（可空字符串）；注册后返回任务ID号 */
    type_TaskId ID_Tim1s = TOSF_LoginTask(task_timing_1s, 1000, 0, "T_1S");
    TOSF_LoginTask(task_feed_iwdg, 3000, 50, "IWDG");

    while (1)
    {
        /* 非阻塞轮询调度 */
        TOSF_TaskHandler();
    }
}

/* 定时器中断中调用，传入定时器中断时间（节拍） */
#include "TeamOSFrame.h"
void TIM2_IRQHandler(void)
{
    TOSF_TimerCB(1);
}
```
